#  Main C Variable Types

## Name

C Variables 


## Description

The main variables in C

* char
* int
* double
* float

## Installation

**linux**

Build binary

```
make
```

Remove object file

```
make clean
```

Remove executabe 

```
make remove
```

Remove object and executable files

```
make clean && remove
```
## Licence
GPL 3 


